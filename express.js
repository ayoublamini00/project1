const express = require('express')
const PORT = process.env.PORT || 8080
const MONGOS = require('mongoose')
const app = express() 
app.get('/hey',(req,res) => {
    res.send('Hello')
})
app.listen(PORT)
MONGOS.connect('mongodb://localhost:27017/expressio')

